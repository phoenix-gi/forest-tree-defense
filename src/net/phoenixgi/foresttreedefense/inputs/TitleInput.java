package net.phoenixgi.foresttreedefense.inputs;

import net.phoenixgi.foresttreedefense.engine.input.StandardInput;
import net.phoenixgi.foresttreedefense.scenes.TitleScene;

import java.awt.event.KeyEvent;

public class TitleInput extends StandardInput {
    TitleScene scene;

    public TitleInput(TitleScene scene) {
        this.scene = scene;
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        super.keyReleased(keyEvent);
        if (keyEvent.getKeyCode() == KeyEvent.VK_SPACE) {
            scene.setMenuScene();
        }
    }
}
