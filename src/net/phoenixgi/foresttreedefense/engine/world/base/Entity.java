package net.phoenixgi.foresttreedefense.engine.world.base;

public abstract class Entity {

    public abstract void setState(EntityState state);

    public abstract void setView(EntityView view);

    public abstract EntityState getState();

    public abstract EntityView getView();
}
