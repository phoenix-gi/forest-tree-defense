package net.phoenixgi.foresttreedefense.engine.graphics;


import java.awt.Graphics2D;

public class Graphics2DBrush extends Brush {
    Graphics2D g2d;

    public Graphics2DBrush(Graphics2D g2d) {
        this.g2d = g2d;
    }

    @Override
    public Graphics2D getRenderObject() {
        return g2d;
    }
}
