package net.phoenixgi.foresttreedefense.inputs;

import net.phoenixgi.foresttreedefense.engine.input.StandardInput;
import net.phoenixgi.foresttreedefense.engine.world.world2d.EntityState2D;
import net.phoenixgi.foresttreedefense.engine.world.world2d.Vector2D;
import net.phoenixgi.foresttreedefense.scenes.TreeDefenseScene;

import java.awt.event.KeyEvent;

public class DefenderInput extends StandardInput {
    TreeDefenseScene scene;

    public DefenderInput(TreeDefenseScene scene) {
        this.scene = scene;
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        EntityState2D state = scene.getDefender().getState();
        Vector2D velocity = state.getVelocity();
        if (scene.getDefender().getState().getHealth() <= 0) {
            velocity.set(0, 0);
            return;
        }

        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_W:
                velocity.y = -100;
                break;
            case KeyEvent.VK_S:
                velocity.y = 100;
                break;
            case KeyEvent.VK_A:
                velocity.x = -100;
                break;
            case KeyEvent.VK_D:
                velocity.x = 100;
                break;
            case KeyEvent.VK_SPACE:
                scene.getDefender().getState().attack();
                break;
        }
        scene.getDefender().getState().getDirection().set(velocity.x, velocity.y).normThis().mulThis(15);
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        EntityState2D state = scene.getDefender().getState();
        Vector2D velocity = state.getVelocity();
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_W:
            case KeyEvent.VK_S:
                velocity.y = 0;
                break;
            case KeyEvent.VK_A:
            case KeyEvent.VK_D:
                velocity.x = 0;
                break;
        }
    }
}
