package net.phoenixgi.foresttreedefense.engine.world.world2d;

public class RectangleBorders2D extends Borders2D {
    private Vector2D firstVertex;
    private Vector2D secondVertex;

    public RectangleBorders2D() {
        this.firstVertex = new Vector2D();
        this.secondVertex = new Vector2D();
    }

    public RectangleBorders2D(Vector2D firstVertex, Vector2D secondVertex) {
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
        if (this.firstVertex == null) {
            this.firstVertex = new Vector2D();
        }
        if (this.secondVertex == null) {
            this.secondVertex = new Vector2D();
        }
    }

    public void setFirstVertex(Vector2D firstVertex) {
        this.firstVertex.set(firstVertex);
    }

    public void setSecondVertex(Vector2D secondVertex) {
        this.secondVertex.set(secondVertex);
    }

    @Override
    public Vector2D getFirstVertex() {
        return firstVertex;
    }

    @Override
    public Vector2D getSecondVertex() {
        return secondVertex;
    }

    @Override
    public boolean isOverlapWith(Borders2D borders2D) {
        if (borders2D instanceof RectangleBorders2D) {
            RectangleBorders2D other = (RectangleBorders2D) borders2D;

            double thisWidth = Math.abs(firstVertex.x - secondVertex.x);
            double thisHeight = Math.abs(firstVertex.y - secondVertex.y);

            double otherWidth = Math.abs(other.firstVertex.x - other.secondVertex.x);
            double otherHeight = Math.abs(other.firstVertex.y - other.secondVertex.y);

            if (this.firstVertex.x < other.firstVertex.x + otherWidth &&
                    this.firstVertex.x + thisWidth > other.firstVertex.x &&
                    this.firstVertex.y < other.firstVertex.y + otherHeight &&
                    this.firstVertex.y + thisHeight > other.firstVertex.y) {
                return true;
            }
        } else {
            throw new IllegalArgumentException("Can't test overlap with " + borders2D.getClass());
        }
        return false;
    }
}