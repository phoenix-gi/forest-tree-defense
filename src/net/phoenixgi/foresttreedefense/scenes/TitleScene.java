package net.phoenixgi.foresttreedefense.scenes;

import net.phoenixgi.foresttreedefense.engine.GameScene;
import net.phoenixgi.foresttreedefense.engine.GameUpdater;
import net.phoenixgi.foresttreedefense.engine.graphics.Brush;
import net.phoenixgi.foresttreedefense.engine.input.GameInput;
import net.phoenixgi.foresttreedefense.inputs.TitleInput;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

public class TitleScene extends GameScene {

    double timeLeft = 0;

    public TitleScene(GameUpdater updater) {
        super(updater);
    }

    public void setMenuScene() {
        getGameUpdater().setScene(new MenuScene(getGameUpdater()));
    }

    @Override
    public void update(double deltaTime) {
        timeLeft += deltaTime;
        if (timeLeft >= 4) {
            setMenuScene();
        }
    }

    @Override
    public void render(Brush brush) {
        Graphics2D g = (Graphics2D) brush.getRenderObject();
        int width = getGameUpdater().getWindow().getCanvas().getJComponent().getWidth();
        int height = getGameUpdater().getWindow().getCanvas().getJComponent().getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, width, height);
        g.setColor(Color.black);
        Font font = new Font("Arial", Font.BOLD, 64);
        Font small = new Font("Arial", Font.ITALIC, 16);
        g.setFont(font);
        FontMetrics metrics = g.getFontMetrics();
        String author = "phoenix-gi";
        String special = "special for Ludum Dare 46";
        g.drawString(author, width / 2 - metrics.stringWidth(author) / 2, height / 2);
        g.setFont(small);
        metrics = g.getFontMetrics();
        g.setColor(new Color(155, 62, 6));
        g.drawString(special, width / 2 - metrics.stringWidth(special) / 2 + 64, height / 2 + 48);
    }

    @Override
    protected void createInput() {
        GameInput gameInput = new TitleInput(this);
        setGameInput(gameInput);
    }
}
