package net.phoenixgi.foresttreedefense.engine.world.world2d;

import net.phoenixgi.foresttreedefense.engine.world.base.Entity;
import net.phoenixgi.foresttreedefense.engine.world.base.World;
import net.phoenixgi.foresttreedefense.engine.world.base.WorldRenderer;
import net.phoenixgi.foresttreedefense.engine.world.base.WorldUpdater;

import java.util.ArrayList;
import java.util.Arrays;

public class World2D extends World {
    private ArrayList<Entity2D> entities;
    private WorldUpdater2D updater;
    private WorldRenderer2D renderer;

    public World2D() {
        entities = new ArrayList<>();
        updater = new WorldUpdater2D(this);
        renderer = new WorldRenderer2D(this);
    }

    @Override
    public void setEntities(Entity[] entities) {
        this.entities.clear();
        this.entities.addAll(Arrays.asList((Entity2D[]) entities));
    }

    public void setEntities(ArrayList<Entity2D> entities) {
        this.entities = entities;
    }

    @Override
    public void setUpdater(WorldUpdater updater) {
        this.updater = (WorldUpdater2D) updater;
    }

    @Override
    public void setRenderer(WorldRenderer renderer) {
        this.renderer = (WorldRenderer2D) renderer;
    }

    @Override
    public Entity2D[] getEntities() {
        return (Entity2D[]) entities.toArray();
    }

    public ArrayList<Entity2D> getEntitiesList() {
        return entities;
    }

    @Override
    public WorldUpdater2D getUpdater() {
        return updater;
    }

    @Override
    public WorldRenderer2D getRenderer() {
        return renderer;
    }
}
