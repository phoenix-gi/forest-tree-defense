package net.phoenixgi.foresttreedefense.entities;

import net.phoenixgi.foresttreedefense.engine.world.world2d.CircleBorders2D;
import net.phoenixgi.foresttreedefense.engine.world.world2d.EntityState2D;
import net.phoenixgi.foresttreedefense.engine.world.world2d.Vector2D;

public class WarriorEntityState2D extends EntityState2D {
    private double health = 100;
    private double healthMax = 100;
    private double attackRadius = 20;
    private boolean attackMade = false;
    private boolean attack = false;
    private double attackDelay = 0.5;
    private double attackTimePassed = 0;
    private double attackPower = 30;
    private Vector2D direction;

    public WarriorEntityState2D() {
        super();
        direction = new Vector2D();
    }

    public void attack() {
        if (!isAttackMade()) {
            attack = true;
            attackTimePassed = 0;
        }
    }

    private void stopAttack() {
        attack = false;
        attackMade = false;
        System.out.println("Weapon recharged!");
    }

    public boolean isInAttack() {
        return attack;
    }

    public boolean isAttackMade() {
        return attackMade;
    }

    public void makeAttack() {
        attackMade = true;
    }

    public double getHealth() {
        return health;
    }

    public double getAttackDelay() {
        return attackDelay;
    }

    public double getAttackRadius() {
        return attackRadius;
    }

    public double getAttackTimePassed() {
        return attackTimePassed;
    }

    public double getAttackPower() {
        return attackPower;
    }

    public double getHealthMax() {
        return healthMax;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public void setAttackDelay(double attackDelay) {
        this.attackDelay = attackDelay;
    }

    public void setAttackRadius(double attackRadius) {
        this.attackRadius = attackRadius;
    }

    public void setAttackTimePassed(double attackTimePassed) {
        this.attackTimePassed = attackTimePassed;
    }

    public void setAttackPower(double attackPower) {
        this.attackPower = attackPower;
    }

    public void setHealthMax(double healthMax) {
        this.healthMax = healthMax;
    }

    public Vector2D getDirection() {
        return direction;
    }

    public void update(double deltaTime) {
        if (attack) {
            attackTimePassed += deltaTime;
            if (attackTimePassed >= attackDelay) {
                stopAttack();
            }
        }
    }

    public boolean isUnderAttack(WarriorEntityState2D other) {
        CircleBorders2D attackBorders = new CircleBorders2D(other.getPosition(), attackRadius);
        return attackBorders.isOverlapWith(this.getBorders2D());
    }

    public void takeDamage(WarriorEntityState2D other) {
        health -= other.attackPower;
        if (health < 0) {
            health = 0;
        }
    }
}
