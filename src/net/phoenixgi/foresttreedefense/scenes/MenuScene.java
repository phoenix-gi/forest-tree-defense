package net.phoenixgi.foresttreedefense.scenes;

import net.phoenixgi.foresttreedefense.Assets;
import net.phoenixgi.foresttreedefense.engine.GameScene;
import net.phoenixgi.foresttreedefense.engine.GameUpdater;
import net.phoenixgi.foresttreedefense.engine.graphics.Brush;
import net.phoenixgi.foresttreedefense.engine.input.GameInput;
import net.phoenixgi.foresttreedefense.inputs.MenuInput;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class MenuScene extends GameScene {

    BufferedImage logo;

    public MenuScene(GameUpdater updater) {
        super(updater);
        logo = Assets.logo;
    }

    public void setTreeDefenseScene() {
        getGameUpdater().setScene(new TreeDefenseScene(getGameUpdater()));
    }

    @Override
    public void update(double deltaTime) {
    }

    @Override
    public void render(Brush brush) {
        Graphics2D g = (Graphics2D) brush.getRenderObject();
        int width = getGameUpdater().getWindow().getCanvas().getJComponent().getWidth();
        int height = getGameUpdater().getWindow().getCanvas().getJComponent().getHeight();
        g.setColor(new Color(141, 240, 127));
        g.fillRect(0, 0, width, height);
        g.drawImage(logo, width / 2 - logo.getWidth() / 2, height / 2 - logo.getHeight() / 2, null);
        Font font = new Font("Arial", Font.BOLD, 16);
        g.setFont(font);
        g.setColor(new Color(25, 114, 36));
        FontMetrics metrics = g.getFontMetrics();
        String pressEnter = "PRESS ENTER TO START";
        g.drawString(pressEnter, width / 2 - metrics.stringWidth(pressEnter) / 2, height - metrics.getHeight());
    }

    @Override
    protected void createInput() {
        GameInput gameInput = new MenuInput(this);
        setGameInput(gameInput);
    }

}
