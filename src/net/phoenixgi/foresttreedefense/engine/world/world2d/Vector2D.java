package net.phoenixgi.foresttreedefense.engine.world.world2d;

public class Vector2D {

    public static double TO_RADIANS = ((1 / 180.0f) * Math.PI);
    public static double TO_DEGREES = ((1 / Math.PI) * 180);
    public double x, y;

    public Vector2D() {
        x = 0;
        y = 0;
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D(Vector2D other) {
        this.x = other.x;
        this.y = other.y;
    }

    public Vector2D copy() {
        return new Vector2D(x, y);
    }

    public Vector2D set(double x, double y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public Vector2D set(Vector2D other) {
        this.x = other.x;
        this.y = other.y;
        return this;
    }

    public Vector2D add(double x, double y) {
        return new Vector2D(this.x + x, this.y + y);
    }

    public Vector2D add(Vector2D other) {
        return new Vector2D(this.x + other.x, this.y + other.y);
    }

    public Vector2D sub(double x, double y) {
        return new Vector2D(this.x - x, this.y - y);
    }

    public Vector2D sub(Vector2D other) {
        return new Vector2D(this.x - other.x, this.y - other.y);
    }

    public Vector2D mul(double scalar) {
        return new Vector2D(this.x * scalar, this.y * scalar);
    }

    public Vector2D addThis(double x, double y) {
        this.x += x;
        this.y += y;
        return this;
    }

    public Vector2D addThis(Vector2D other) {
        this.x += other.x;
        this.y += other.y;
        return this;
    }

    public Vector2D subThis(double x, double y) {
        this.x -= x;
        this.y -= y;
        return this;
    }

    public Vector2D subThis(Vector2D other) {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }

    public Vector2D mulThis(double scalar) {
        this.x *= scalar;
        this.y *= scalar;
        return this;
    }

    public double len() {
        return Math.sqrt(x * x + y * y);
    }

    public double lenSqr() {
        return x * x + y * y;
    }

    public Vector2D normThis() {
        double len = len();
        if (len != 0) {
            this.x /= len;
            this.y /= len;
        }
        return this;
    }

    public double angle() {
        double angle = Math.atan2(y, x) * TO_DEGREES;
        if (angle < 0) {
            angle += 360;
        }
        return angle;
    }

    public Vector2D rotateThis(double angle) {
        double rad = angle * TO_RADIANS;
        double cos = Math.cos(rad);
        double sin = Math.sin(rad);

        double newX = this.x * cos - this.y * sin;
        double newY = this.x * sin + this.y * cos;

        this.x = newX;
        this.y = newY;

        return this;
    }

    public Vector2D rotate(double angle) {
        double rad = angle * TO_RADIANS;
        double cos = Math.cos(rad);
        double sin = Math.sin(rad);

        double newX = this.x * cos - this.y * sin;
        double newY = this.x * sin + this.y * cos;

        return new Vector2D(newX, newY);
    }

    public double dist(Vector2D other) {
        double distX = this.x - other.x;
        double distY = this.y - other.y;
        return Math.sqrt(distX * distX + distY * distY);
    }

    public double dist(double x, double y) {
        double distX = this.x - x;
        double distY = this.y - y;
        return Math.sqrt(distX * distX + distY * distY);
    }

    public double distSquared(Vector2D other) {
        double distX = this.x - other.x;
        double distY = this.y - other.y;
        return distX * distX + distY * distY;
    }

    public double distSquared(double x, double y) {
        double distX = this.x - x;
        double distY = this.y - y;
        return distX * distX + distY * distY;
    }

    public double dot(Vector2D other) {
        return x * other.x + y * other.y;
    }

}