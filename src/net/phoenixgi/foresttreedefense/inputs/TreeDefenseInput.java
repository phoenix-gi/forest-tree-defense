package net.phoenixgi.foresttreedefense.inputs;

import net.phoenixgi.foresttreedefense.engine.input.StandardInput;
import net.phoenixgi.foresttreedefense.scenes.TreeDefenseScene;

import java.awt.event.KeyEvent;

public class TreeDefenseInput extends StandardInput {
    TreeDefenseScene scene;
    DefenderInput defenderInput;

    public TreeDefenseInput(TreeDefenseScene scene) {
        this.scene = scene;
        defenderInput = new DefenderInput(scene);
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        defenderInput.keyPressed(keyEvent);
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        defenderInput.keyReleased(keyEvent);
    }
}
