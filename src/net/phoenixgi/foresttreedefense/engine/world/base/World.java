package net.phoenixgi.foresttreedefense.engine.world.base;

public abstract class World {
    public abstract void setEntities(Entity[] entities);

    public abstract void setUpdater(WorldUpdater updater);

    public abstract void setRenderer(WorldRenderer renderer);

    public abstract Entity[] getEntities();

    public abstract WorldUpdater getUpdater();

    public abstract WorldRenderer getRenderer();
}
