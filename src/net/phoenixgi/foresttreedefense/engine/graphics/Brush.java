package net.phoenixgi.foresttreedefense.engine.graphics;

public abstract class Brush {
    public abstract Object getRenderObject();
}