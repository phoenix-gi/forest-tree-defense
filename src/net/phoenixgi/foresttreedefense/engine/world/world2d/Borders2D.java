package net.phoenixgi.foresttreedefense.engine.world.world2d;

public abstract class Borders2D {
    public abstract boolean isOverlapWith(Borders2D borders2D);

    public abstract Vector2D getFirstVertex();

    public abstract Vector2D getSecondVertex();
}
