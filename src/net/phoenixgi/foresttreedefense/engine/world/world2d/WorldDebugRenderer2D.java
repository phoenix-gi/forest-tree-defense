package net.phoenixgi.foresttreedefense.engine.world.world2d;

import java.awt.*;

public class WorldDebugRenderer2D extends WorldRenderer2D {
    public WorldDebugRenderer2D(World2D world) {
        super(world);
    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(Color.black);
        g.drawLine(0, 0, 100, 0);
        g.drawLine(0, 0, 0, 100);
        g.drawString("x", 110, 0);
        g.drawString("y", 0, 110);
        super.render(g);
    }

    @Override
    protected void renderEntity2D(Entity2D entity) {
        Graphics2D g = getGraphics2D();
        Borders2D borders = entity.getState().getBorders2D();
        g.setColor(entity.getView().getDebugColor());

        int x = (int) (borders.getFirstVertex().x);
        int y = (int) (borders.getFirstVertex().y);
        int width = (int) Math.abs(borders.getFirstVertex().x - borders.getSecondVertex().x);
        int height = (int) Math.abs(borders.getFirstVertex().y - borders.getSecondVertex().y);
        if (borders instanceof CircleBorders2D) {
            Vector2D center = ((CircleBorders2D) borders).getCenter();
            double radius = ((CircleBorders2D) borders).getRadius();
            g.drawArc(x, y, width, height, 0, 360);
            g.drawLine((int) (center.x - radius), (int) (center.y), (int) (center.x + radius), (int) (center.y));
            g.drawLine((int) (center.x), (int) (center.y - radius), (int) (center.x), (int) (center.y + radius));
        } else if (borders instanceof RectangleBorders2D) {
            g.drawRect(x, y, width, height);
            g.drawLine(x, y, x + width, y + height);
            g.drawLine(x + width, y, x, y + height);
        }
    }
}
