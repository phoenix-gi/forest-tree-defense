package net.phoenixgi.foresttreedefense.scenes;

import net.phoenixgi.foresttreedefense.engine.GameScene;
import net.phoenixgi.foresttreedefense.engine.GameUpdater;
import net.phoenixgi.foresttreedefense.engine.graphics.Brush;
import net.phoenixgi.foresttreedefense.engine.input.GameInput;
import net.phoenixgi.foresttreedefense.engine.world.world2d.*;
import net.phoenixgi.foresttreedefense.entities.*;
import net.phoenixgi.foresttreedefense.inputs.TreeDefenseInput;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class TreeDefenseScene extends GameScene {
    World2D world;
    TreeEntity2D tree;
    DefenderEntity2D defender;
    ArrayList<EnemyEntity2D> enemies;
    ArrayList<EnemyController> controllers;

    public TreeDefenseScene(GameUpdater updater) {
        super(updater);
        initializeWorld();
    }

    public void initializeWorld() {
        world = new World2D();
        WorldDebugRenderer2D worldDebugRenderer2D = new WorldDebugRenderer2D(world);
        world.setRenderer(worldDebugRenderer2D);
        ArrayList<Entity2D> entities = new ArrayList<>();
        tree = new TreeEntity2D();
        tree.getState().setBorders2D(new CircleBorders2D(new Vector2D(), 50));
        tree.getState().setHealthMax(1000);
        tree.getState().setHealth(1000);


        defender = new DefenderEntity2D();
        defender.getState().getPosition().set(0, 80);
        defender.getState().setBorders2D(new CircleBorders2D(new Vector2D(0, 80), 10));

        entities.add(tree);
        entities.add(defender);

        enemies = new ArrayList<>();
        controllers = new ArrayList<>();
        int n = 10;
        double dist = 200;
        double angle = 0;
        for (int i = 0; i < n; i++) {
            EnemyEntity2D enemy = new EnemyEntity2D();
            angle += 2 * Math.PI / n;
            double x = (dist + Math.random() * 100) * Math.cos(angle);
            double y = (dist + Math.random() * 100) * Math.sin(angle);
            enemy.getState().setBorders2D(new CircleBorders2D(new Vector2D(x, y), 10));
            enemy.getState().getPosition().set(x, y);
            enemy.getState().setAttackPower(5);
            enemy.getState().setAttackDelay(1);
            enemies.add(enemy);
            controllers.add(new EnemyController(enemy, tree, defender));
        }
        entities.addAll(enemies);

        world.setEntities(entities);
    }

    public DefenderEntity2D getDefender() {
        return defender;
    }

    @Override
    public void update(double deltaTime) {
        for (EnemyController controller : controllers) {
            controller.update();
        }
        world.getUpdater().update(deltaTime);

        if (defender.getState().isInAttack() && !defender.getState().isAttackMade()) {
            for (EnemyEntity2D enemy : enemies) {
                if (enemy.getState().isUnderAttack(defender.getState())) {
                    enemy.getState().takeDamage(defender.getState());
                }
            }
            defender.getState().makeAttack();
            System.out.println("Defender attacks!");
        }
        defender.getState().update(deltaTime);

        for (EnemyEntity2D enemy : enemies) {
            if (enemy.getState().isInAttack() && !enemy.getState().isAttackMade()) {
                if (defender.getState().isUnderAttack(enemy.getState())) {
                    defender.getState().takeDamage(enemy.getState());
                }
                if (tree.getState().isUnderAttack(enemy.getState())) {
                    tree.getState().takeDamage(enemy.getState());
                }
                enemy.getState().makeAttack();
                System.out.println("Enemy attacks!");
            }
            enemy.getState().update(deltaTime);
        }
    }

    @Override
    public void render(Brush brush) {
        Graphics2D g = (Graphics2D) brush.getRenderObject();
        int width = getGameUpdater().getWindow().getCanvas().getJComponent().getWidth();
        int height = getGameUpdater().getWindow().getCanvas().getJComponent().getHeight();
        g.setColor(new Color(190, 255, 132));
        g.fillRect(0, 0, width, height);
        g.translate(width / 2, height / 2);
        world.getRenderer().render(g);

        for (EnemyEntity2D enemy : enemies) {
            renderDebugWarriorState(g, enemy);
        }
        renderDebugWarriorState(g, defender);
        renderDebugWarriorState(g, tree);
        g.translate(-width / 2, -height / 2);
    }

    private void renderDebugWarriorState(Graphics2D g, Entity2D entity) {
        WarriorEntityState2D state = (WarriorEntityState2D) entity.getState();
        Vector2D pos = state.getPosition();
        Vector2D dir = state.getDirection();
        if (state.isInAttack()) {
            double radius = state.getAttackRadius() * (1 - state.getAttackTimePassed() / state.getAttackDelay());
            g.setColor(Color.RED);
            g.drawArc((int) (pos.x - radius), (int) (pos.y - radius), (int) (radius * 2), (int) (radius * 2), 0, 360);
        }
        double hp = state.getHealth();
        if (hp > 0) {
            double hpMax = state.getHealthMax();
            CircleBorders2D circle = (CircleBorders2D) state.getBorders2D();
            double radius = circle.getRadius();
            g.fillRect((int) (pos.x - radius), (int) (pos.y - 5 - radius), (int) (2 * radius * hp / hpMax), 4);
        }
        g.setColor(Color.RED);
        g.drawLine((int) (pos.x), (int) (pos.y), (int) (pos.x + dir.x), (int) (pos.y + dir.y));
    }

    @Override
    protected void createInput() {
        GameInput gameInput = new TreeDefenseInput(this);
        setGameInput(gameInput);
    }
}
