package net.phoenixgi.foresttreedefense;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Assets {
    public static BufferedImage logo;

    public static void load() {
        try {
            logo = ImageIO.read(Assets.class.getResource("/assets/logo.png"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}