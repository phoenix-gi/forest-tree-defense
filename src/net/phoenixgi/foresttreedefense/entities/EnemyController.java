package net.phoenixgi.foresttreedefense.entities;

import net.phoenixgi.foresttreedefense.engine.world.world2d.CircleBorders2D;
import net.phoenixgi.foresttreedefense.engine.world.world2d.Vector2D;

public class EnemyController {
    EnemyEntity2D enemy;
    TreeEntity2D tree;
    DefenderEntity2D defender;

    public EnemyController(EnemyEntity2D enemy, TreeEntity2D tree, DefenderEntity2D defender) {
        this.enemy = enemy;
        this.tree = tree;
        this.defender = defender;
    }

    public void update() {
        if (enemy.getState().getHealth() <= 0) {
            enemy.getState().getVelocity().set(0, 0);
            return;
        }
        Vector2D treePos = tree.getState().getPosition();
        Vector2D enemyPos = enemy.getState().getPosition();
        Vector2D defenderPos = defender.getState().getPosition();
        Vector2D treeVel = tree.getState().getVelocity();
        Vector2D enemyVel = enemy.getState().getVelocity();
        Vector2D defenderVel = defender.getState().getVelocity();
        Vector2D defenderDir = defender.getState().getDirection();

        double distToTree = enemyPos.dist(treePos);
        double distToDefender = enemyPos.dist(defenderPos);
        if (distToDefender < ((CircleBorders2D) defender.getState().getBorders2D()).getRadius() + enemy.getState().getAttackRadius()) {
            if (defender.getState().getHealth() > 0) {
                enemy.getState().attack();
            }
        }

        if (distToTree < ((CircleBorders2D) tree.getState().getBorders2D()).getRadius() + enemy.getState().getAttackRadius()) {
            if (tree.getState().getHealth() > 0) {
                enemy.getState().attack();
            }
        }

        if (distToTree < distToDefender + ((CircleBorders2D) tree.getState().getBorders2D()).getRadius() && tree.getState().getHealth() > 0) {
            Vector2D velocity = treePos.sub(enemyPos).normThis().mulThis(20);
            enemy.getState().getVelocity().set(velocity);
            enemy.getState().getDirection().set(velocity.x, velocity.y).normThis().mulThis(15);
        } else {
            enemy.getState().getVelocity().set(0, 0);
            Vector2D divider = enemyPos.sub(defenderPos).rotateThis(-Math.PI / 2);
            double dot = divider.dot(defenderDir);
            if (dot < 0) {
                // Go to tree or defender
                Vector2D velocity = new Vector2D();
                if (tree.getState().getHealth() > 0) {
                    velocity.set(treePos.sub(enemyPos).normThis().mulThis(20));
                } else {
                    velocity.set(defenderPos.sub(enemyPos).normThis().mulThis(20));
                }
                enemy.getState().getVelocity().set(velocity);
                enemy.getState().getDirection().set(velocity.x, velocity.y).normThis().mulThis(15);
            } else {
                // Run away
                Vector2D velocity = enemyPos.sub(defenderPos).normThis().mulThis(20);
                if (defender.getState().getHealth() < enemy.getState().getHealth()) {
                    velocity.mulThis(-1);
                }
                enemy.getState().getVelocity().set(velocity);
                enemy.getState().getDirection().set(velocity.x, velocity.y).normThis().mulThis(15);
            }

        }


    }
}
