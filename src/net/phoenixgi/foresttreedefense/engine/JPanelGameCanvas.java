package net.phoenixgi.foresttreedefense.engine;

import net.phoenixgi.foresttreedefense.engine.graphics.Brush;
import net.phoenixgi.foresttreedefense.engine.graphics.Graphics2DBrush;
import net.phoenixgi.foresttreedefense.engine.input.GameInput;

import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;

public class JPanelGameCanvas extends GameCanvas {

    JPanel panel;
    BufferedImage image;
    GameScene currentScene;

    public JPanelGameCanvas() {
        buildPanel();
    }

    private void buildPanel() {
        panel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                int width = getWidth();
                int height = getHeight();
                image = (BufferedImage) createImage(width, height);
                java.awt.Graphics2D g2d = image.createGraphics();
                Brush brush = new Graphics2DBrush(g2d);
                if (currentScene != null) {
                    currentScene.render(brush);
                }
                g.drawImage(image, 0, 0, this);
            }
        };
    }

    @Override
    public void render(GameScene scene) {
        this.currentScene = scene;
        panel.repaint();
    }

    @Override
    public JComponent getJComponent() {
        return panel;
    }

    @Override
    public void changeInputListeners(GameInput gameInput) {
        if (currentScene != null) {
            panel.removeKeyListener((KeyListener) currentScene.getGameInput());
            panel.removeMouseListener((MouseListener) currentScene.getGameInput());
            panel.removeMouseWheelListener((MouseWheelListener) currentScene.getGameInput());
            panel.removeMouseMotionListener((MouseMotionListener) currentScene.getGameInput());
        }

        panel.addKeyListener((KeyListener) gameInput);
        panel.addMouseListener((MouseListener) gameInput);
        panel.addMouseWheelListener((MouseWheelListener) gameInput);
        panel.addMouseMotionListener((MouseMotionListener) gameInput);
    }
}
