package net.phoenixgi.foresttreedefense.engine;

import net.phoenixgi.foresttreedefense.engine.graphics.Brush;
import net.phoenixgi.foresttreedefense.engine.input.GameInput;

public abstract class GameScene {

    protected GameUpdater updater;
    protected GameInput gameInput;

    public GameScene(GameUpdater updater) {
        this.updater = updater;
        createInput();
    }

    public abstract void update(double deltaTime);

    public abstract void render(Brush brush);

    public GameUpdater getGameUpdater() {
        return updater;
    }

    protected abstract void createInput();

    protected void setGameInput(GameInput gameInput) {
        this.gameInput = gameInput;
    }

    public GameInput getGameInput() {
        return gameInput;
    }
}
