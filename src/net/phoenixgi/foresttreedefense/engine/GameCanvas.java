package net.phoenixgi.foresttreedefense.engine;

import net.phoenixgi.foresttreedefense.engine.input.GameInput;

import javax.swing.JComponent;

public abstract class GameCanvas {

    public abstract void render(GameScene scene);

    public abstract JComponent getJComponent();

    public abstract void changeInputListeners(GameInput gameInput);
}
