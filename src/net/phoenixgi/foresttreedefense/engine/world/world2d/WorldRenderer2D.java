package net.phoenixgi.foresttreedefense.engine.world.world2d;

import net.phoenixgi.foresttreedefense.engine.world.base.WorldRenderer;

import java.awt.Graphics2D;

public class WorldRenderer2D extends WorldRenderer {
    private World2D world;
    private Graphics2D g;

    public WorldRenderer2D(World2D world) {
        this.world = world;
    }

    @Override
    public void render() {
        for (Entity2D entity : world.getEntitiesList()) {
            renderEntity2D(entity);
        }
    }

    public Graphics2D getGraphics2D() {
        return g;
    }

    protected void renderEntity2D(Entity2D entity) {
        Sprite2D sprite = entity.getView().getSprite2D();
        if (sprite != null) {
            sprite.draw(g, (int) entity.getState().getPosition().x, (int) entity.getState().getPosition().y);
        }
    }

    public void render(Graphics2D g) {
        this.g = g;
        render();
    }
}