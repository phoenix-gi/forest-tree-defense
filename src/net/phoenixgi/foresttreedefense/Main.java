package net.phoenixgi.foresttreedefense;

import net.phoenixgi.foresttreedefense.engine.GameCanvas;
import net.phoenixgi.foresttreedefense.engine.GameScene;
import net.phoenixgi.foresttreedefense.engine.GameUpdater;
import net.phoenixgi.foresttreedefense.engine.GameWindow;
import net.phoenixgi.foresttreedefense.engine.JPanelGameCanvas;
import net.phoenixgi.foresttreedefense.scenes.TitleScene;

public class Main {
    public static void main(String[] args) {
        Assets.load();
        GameCanvas canvas = new JPanelGameCanvas();
        GameWindow window = new GameWindow(canvas, "Forest Tree Defense", 800, 600);
        GameUpdater updater = new GameUpdater(window);
        GameScene titleScene = new TitleScene(updater);
        updater.setScene(titleScene);
        updater.start();
    }
}
