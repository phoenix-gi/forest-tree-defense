package net.phoenixgi.foresttreedefense.engine.world.base;

public abstract class WorldUpdater {
    public abstract void update();
}
