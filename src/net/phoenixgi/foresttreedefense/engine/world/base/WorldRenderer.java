package net.phoenixgi.foresttreedefense.engine.world.base;

public abstract class WorldRenderer {
    public abstract void render();
}
