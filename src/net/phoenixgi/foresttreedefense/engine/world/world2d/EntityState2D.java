package net.phoenixgi.foresttreedefense.engine.world.world2d;

import net.phoenixgi.foresttreedefense.engine.world.base.EntityState;

public class EntityState2D extends EntityState {
    Vector2D position;
    Vector2D velocity;
    Borders2D borders2D;

    public EntityState2D() {
        position = new Vector2D();
        velocity = new Vector2D();
    }

    public void setPosition(Vector2D position) {
        this.position = position;
    }

    public void setVelocity(Vector2D velocity) {
        this.velocity = velocity;
    }

    public void setBorders2D(Borders2D borders2D) {
        this.borders2D = borders2D;
    }

    public Vector2D getPosition() {
        return position;
    }

    public Vector2D getVelocity() {
        return velocity;
    }

    public Borders2D getBorders2D() {
        return borders2D;
    }
}
