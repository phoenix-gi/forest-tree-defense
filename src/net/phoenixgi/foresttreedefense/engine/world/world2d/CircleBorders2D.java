package net.phoenixgi.foresttreedefense.engine.world.world2d;

public class CircleBorders2D extends Borders2D {

    protected Vector2D center;
    protected double radius;

    public CircleBorders2D(Vector2D center, double radius) {
        this.center = center;
        this.radius = radius;
        if (this.center == null) {
            this.center = new Vector2D();
        }
    }

    public CircleBorders2D() {
        this.center = new Vector2D();
        radius = 10;
    }

    @Override
    public boolean isOverlapWith(Borders2D borders2D) {
        if (borders2D instanceof CircleBorders2D) {
            CircleBorders2D other = (CircleBorders2D) borders2D;
            if (this.center.distSquared(other.center) <= (radius + other.radius) * (radius + other.radius)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Vector2D getFirstVertex() {
        return new Vector2D(center.x - radius, center.y - radius);
    }

    @Override
    public Vector2D getSecondVertex() {
        return new Vector2D(center.x + radius, center.y + radius);
    }

    public Vector2D getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }
}
