package net.phoenixgi.foresttreedefense.engine.world.world2d;

import net.phoenixgi.foresttreedefense.engine.world.base.Entity;
import net.phoenixgi.foresttreedefense.engine.world.base.WorldUpdater;

import java.util.ArrayList;

public class WorldUpdater2D extends WorldUpdater {
    private World2D world;
    private double deltaTime;

    public WorldUpdater2D(World2D world) {
        this.world = world;
        this.deltaTime = 0;
    }

    @Override
    public void update() {
        for (Entity2D entity : world.getEntitiesList()) {
            updateEntity2D(entity);
        }

        ArrayList<Entity2D> overlapped = new ArrayList<>();
        for (Entity2D e1 : world.getEntitiesList()) {
            for (Entity2D e2 : world.getEntitiesList()) {
                if (e1 != e2) {
                    if (e1.getState().getBorders2D().isOverlapWith(e2.getState().getBorders2D())) {
                        if (!overlapped.contains(e1)) {
                            overlapped.add(e1);
                        }
                        if (!overlapped.contains(e2)) {
                            overlapped.add(e2);
                        }
                    }
                }
            }
        }

        deltaTime = -deltaTime;
        for (Entity2D entity : overlapped) {
            updateEntity2D(entity);
        }
        deltaTime = -deltaTime;
    }

    private void updateEntity2D(Entity2D entity) {
        if (entity.getView().getSprite2D() != null) {
            entity.getView().getSprite2D().getSpriteAnimation().update(deltaTime);
        }
        EntityState2D s = entity.getState();
        s.setPosition(s.getPosition().add(s.getVelocity().mul(deltaTime)));
        if (s.getBorders2D() instanceof RectangleBorders2D) {
            s.getBorders2D().getFirstVertex().set(s.getBorders2D().getFirstVertex().add(s.getVelocity().mul(deltaTime)));
            s.getBorders2D().getSecondVertex().set(s.getBorders2D().getSecondVertex().add(s.getVelocity().mul(deltaTime)));
        } else if (s.getBorders2D() instanceof CircleBorders2D) {
            Vector2D circleCenter = ((CircleBorders2D) s.getBorders2D()).getCenter();
            circleCenter.set(circleCenter.add(s.getVelocity().mul(deltaTime)));
        }
    }

    public void update(double deltaTime) {
        this.deltaTime = deltaTime;
        update();
    }
}
