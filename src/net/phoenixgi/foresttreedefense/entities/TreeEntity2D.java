package net.phoenixgi.foresttreedefense.entities;

import net.phoenixgi.foresttreedefense.engine.world.world2d.Entity2D;

import java.awt.Color;

public class TreeEntity2D extends Entity2D {
    public TreeEntity2D() {
        super();
        getView().setDebugColor(new Color(10, 62, 31));
        this.setState(new WarriorEntityState2D());
    }

    @Override
    public WarriorEntityState2D getState() {
        return (WarriorEntityState2D) super.getState();
    }
}
