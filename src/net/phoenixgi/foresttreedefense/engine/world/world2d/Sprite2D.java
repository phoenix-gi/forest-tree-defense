package net.phoenixgi.foresttreedefense.engine.world.world2d;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Sprite2D {

    BufferedImage image;

    Sprite2DAnimation animation;

    public Sprite2D(BufferedImage image) {
        this.image = image;
        animation = new Sprite2DAnimation(image.getWidth(), image.getWidth(), image.getHeight(), 0, 1);
    }

    public Sprite2D(BufferedImage image, Sprite2DAnimation animation) {
        this(image);
        this.animation = animation;
    }

    public void draw(Graphics2D g, int x, int y) {
        int[][] frameProps = animation.getFrameProps();
        int[] frameProp = frameProps[animation.getCurrentFrameNum()];

        g.drawImage(image, x, y, x + frameProp[0], y + frameProp[1], frameProp[2], frameProp[3], frameProp[4], frameProp[5], null);
    }

    public Sprite2DAnimation getSpriteAnimation() {
        return animation;
    }

    public void setSpriteAnimation(Sprite2DAnimation animation) {
        this.animation = animation;
    }
}
