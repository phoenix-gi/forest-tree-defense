package net.phoenixgi.foresttreedefense.entities;

import net.phoenixgi.foresttreedefense.engine.world.world2d.Entity2D;

import java.awt.Color;

public class EnemyEntity2D extends Entity2D {
    public EnemyEntity2D() {
        super();
        getView().setDebugColor(Color.RED);
        this.setState(new WarriorEntityState2D());
    }

    @Override
    public WarriorEntityState2D getState() {
        return (WarriorEntityState2D) super.getState();
    }
}
