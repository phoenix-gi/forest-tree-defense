package net.phoenixgi.foresttreedefense.engine.world.world2d;

import net.phoenixgi.foresttreedefense.engine.world.base.Entity;
import net.phoenixgi.foresttreedefense.engine.world.base.EntityState;
import net.phoenixgi.foresttreedefense.engine.world.base.EntityView;

public class Entity2D extends Entity {
    private EntityState2D state;
    private EntityView2D view;

    public Entity2D() {
        this.state = new EntityState2D();
        this.view = new EntityView2D();
    }

    @Override
    public void setState(EntityState state) {
        this.state = (EntityState2D) state;
    }

    @Override
    public void setView(EntityView view) {
        this.view = (EntityView2D) view;
    }

    @Override
    public EntityState2D getState() {
        return state;
    }

    @Override
    public EntityView2D getView() {
        return view;
    }
}
