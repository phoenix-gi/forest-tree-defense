package net.phoenixgi.foresttreedefense.engine.world.world2d;

import net.phoenixgi.foresttreedefense.engine.world.base.EntityView;

import java.awt.Color;

public class EntityView2D extends EntityView {
    Color debugColor;
    Sprite2D sprite2D;

    public EntityView2D() {
        debugColor = Color.black;
    }

    public void setDebugColor(Color debugColor) {
        this.debugColor = debugColor;
    }

    public Color getDebugColor() {
        return debugColor;
    }

    public void setSprite2D(Sprite2D sprite2D) {
        this.sprite2D = sprite2D;
    }

    public Sprite2D getSprite2D() {
        return sprite2D;
    }
}
