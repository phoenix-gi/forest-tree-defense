package net.phoenixgi.foresttreedefense.inputs;

import net.phoenixgi.foresttreedefense.engine.input.StandardInput;
import net.phoenixgi.foresttreedefense.scenes.MenuScene;

import java.awt.event.KeyEvent;

public class MenuInput extends StandardInput {
    MenuScene scene;

    public MenuInput(MenuScene scene) {
        this.scene = scene;
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        super.keyReleased(keyEvent);
        if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
            scene.setTreeDefenseScene();
        }
    }
}
